﻿using System;
using System.Collections.Generic;
using Entities;
using Rewards.Data;

namespace Rewards.Services
{
    public class UserFeatureUsageService
    {
        private Repository _repository;

        public UserFeatureUsageService()
        {
            _repository = new Repository();
        }
        public void InitializeQuestionAndAnswers()
        {
            _repository.InitializeQuestionAndAnswers();
        }

        public void AddUserProviderUsage(string providerId, string userId)
        {
            _repository.AddUserUsage(new UserProviderUsage
            {
                Id = Guid.NewGuid().ToString(),
                ProviderId = providerId,
                Timestamp = DateTime.UtcNow,
                UserId = userId
            });
        }

        public List<UserProviderUsage> GetUserProviderUsage()
        {
            return _repository.GetAllUserUsage();
        }

        public List<ProviderSurveyQuestion> GetProviderSurveyQuestion(string providerId)
        {
            return _repository.GetProviderQuestions(providerId);
        }

        public List<SurveyQuestion> GetSurveyQuestions(string seriesId)
        {
            return _repository.GetSurveyQuestionsForProvider(seriesId);
        }

        public List<SurveyQuestionAnswer> GetSurveyQuestionAnswers(string questionId)
        {
            return _repository.GetSurveyQuestionAnswers(questionId);
        }


        public void SaveUserFreeTextAnswer(string providerQuestionsId, string questionsId, string userId, string answerId, string userFreeAnswer)
        {
            _repository.AddUserAnswer(new UserSurveyAnswer
            {
                ProviderSurveyQuestionId = providerQuestionsId,
                QuestionId = questionsId,
                Text = userFreeAnswer,
                AnswerId = answerId,
                Id = Guid.NewGuid().ToString(),
                UserId = userId
            });

        }

        public void SaveUserAnswer(string providerQuestionsId,
            string questionsId, string userId, List<SurveyQuestionAnswer> userAnswers)
        {
            foreach (var answer in userAnswers)
            {
                _repository.AddUserAnswer(new UserSurveyAnswer
                {
                    ProviderSurveyQuestionId = providerQuestionsId,
                    QuestionId = questionsId,
                    AnswerId = answer.Id,
                    Id = Guid.NewGuid().ToString(),
                    UserId = userId
                });

            }
        }

        public void DeleteUserAnsweredSurvey(string questionSeriesId)
        {
            _repository.DeleteAnsweredSurvey(questionSeriesId);
        }

        public void DeleteUserPreviousAnswer(string userId, string currentQuestionId)
        {
           _repository.DeleteUserAnswer(userId, currentQuestionId);
        } 
        
        public bool IsSurveyAvailableForProvider(string userId, string providerId)
        {
           return _repository.IsSurveyAvailableForProvider(userId, providerId);
        }
    }
}
