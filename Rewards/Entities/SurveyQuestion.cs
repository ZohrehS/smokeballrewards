﻿using System;

namespace Entities
{
    public class SurveyQuestion
    {
        public string Id { get; set; }
        public string ProviderSurveyQuestionId { get; set; }
        public int Order { get; set; }
        public string PreviousQuestionAnswerId { get; set; }
        public string Text { get; set; }
        public bool CanHaveMoreThanOneAnswer { get; set; }
    }
}
