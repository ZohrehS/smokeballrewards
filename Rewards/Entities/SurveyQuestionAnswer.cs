﻿using System;

namespace Entities
{
    public class SurveyQuestionAnswer
    {
        public string Id { get; set; }
        public string QuestionId { get; set; }
        public int Order { get; set; }
        public string Text { get; set; }
        public bool IsPreDefined { get; set; }
    }
}
