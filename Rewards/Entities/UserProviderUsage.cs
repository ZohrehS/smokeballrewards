﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class UserProviderUsage
    {
        public string Id { get; set; }
        public string ProviderId { get; set; }
        public string UserId { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
