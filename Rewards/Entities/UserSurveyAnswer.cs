﻿using System;

namespace Entities
{
    public class UserSurveyAnswer
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ProviderSurveyQuestionId { get; set; }
        public string QuestionId { get; set; }
        public string AnswerId { get; set; }
        public string Text { get; set; }

    }
}
