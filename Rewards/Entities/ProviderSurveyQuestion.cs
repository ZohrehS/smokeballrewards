﻿using System;

namespace Entities
{
    public class ProviderSurveyQuestion
    {
        public string Id { get; set; }
        public string ProviderId { get; set; }
        public int Order { get; set; }
        public int Credit { get; set; }
    }
}
