﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Rewards.ViewModels
{
    public abstract class NotificationObjectBase
    {
        protected abstract void RaisePropertyChanged(string propertyName);

        protected virtual void RaisePropertyChanged(params string[] propertyNames)
        {
            foreach (string propertyName in propertyNames)
                this.RaisePropertyChanged(propertyName);
        }
        
        protected virtual bool SetValue<T>(ref T target, T value, params string[] propertyNames)
        {
            if (object.Equals((object)target, (object)value))
                return false;
            target = value;
            this.RaisePropertyChanged(propertyNames);
            return true;
        }
    }

    public abstract class NotificationObject : NotificationObjectBase, INotifyPropertyChanged
    {
        public virtual NotificationObject Clone()
        {
            return (NotificationObject)this.MemberwiseClone();
        }

        protected override void RaisePropertyChanged(string propertyName)
        {
            if (propertyName == null)
                throw new ArgumentNullException(nameof(propertyName));
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged == null)
                return;
            propertyChanged((object)this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
