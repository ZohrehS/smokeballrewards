﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Entities;
using Rewards.Services;

namespace Rewards.ViewModels
{
    internal class QuestionsViewModel : NotificationObject
    {
        private string _questionText;
        private List<UserAnswerItem> _answers;
        private UserFeatureUsageService _service;
        private List<SurveyQuestion> _questions;
        private SurveyQuestion _currentQuestion;
        private SurveyQuestion _previousQuestion;
        private bool _isMultiOptionQuestion;
        private string _userFreeAnswerToQuestion;
        private bool _isSingleAnswer;
        private bool _answerIsFreeText;
        private bool _hasMoreQuestions;
        private string _creditWonText;
        private ProviderSurveyQuestion _currentQuestionSeries;
        private string _userId;
        private bool _isBackButtonVisible;
        private bool _isCreditImageVisiblle;

        public QuestionsViewModel()
        {
            _service = new UserFeatureUsageService();
        }
        
        public string QuestionText
        {
            get => _questionText;
            set => SetValue(ref _questionText, value, nameof(QuestionText));
        }

        public bool IsCreditImageVisiblle
        {
            get => _isCreditImageVisiblle;
            set => SetValue(ref _isCreditImageVisiblle, value, nameof(IsCreditImageVisiblle));
        }

        public string CreditWonText
        {
            get => _creditWonText;
            set
            {
                SetValue(ref _creditWonText, value, nameof(CreditWonText));
                IsCreditImageVisiblle = !string.IsNullOrWhiteSpace(value);
            }
        }

        public List<UserAnswerItem> Answers
        {
            get => _answers;
            set => SetValue(ref _answers, value, nameof(Answers));
        }

        public bool IsMultiOptionQuestion
        {
            get => _isMultiOptionQuestion;
            set => SetValue(ref _isMultiOptionQuestion, value, nameof(IsMultiOptionQuestion), nameof(IsSingleAnswer));
        }

        public string UserFreeAnswerToQuestion
        {
            get => _userFreeAnswerToQuestion;
            set => SetValue(ref _userFreeAnswerToQuestion, value, nameof(UserFreeAnswerToQuestion));
        }

        public bool IsSingleAnswer
        {
            get => _isSingleAnswer;
            set => SetValue(ref _isSingleAnswer, value, nameof(IsSingleAnswer), nameof(IsMultiAnswer));
        }

        public bool IsMultiAnswer => !IsSingleAnswer;

        public bool AnswerIsFreeText
        {
            get => _answerIsFreeText;
            set => SetValue(ref _answerIsFreeText, value, nameof(AnswerIsFreeText));
        }

        public bool HasMoreQuestions
        {
            get => _hasMoreQuestions;
            set => SetValue(ref _hasMoreQuestions, value, nameof(HasMoreQuestions), nameof(NoMoreQuestions));
        }

        public bool IsBackButtonVisible
        {
            get => _isBackButtonVisible;
            set => SetValue(ref _isBackButtonVisible, value, nameof(IsBackButtonVisible));
        }
        
        public bool NoMoreQuestions => !HasMoreQuestions;

        public void Initialize()
        {
            IsBackButtonVisible = false;
            var userUsage = _service.GetUserProviderUsage().OrderByDescending(i=>i.Timestamp).FirstOrDefault();

            if (userUsage == null)
            {
                QuestionText = "There is no survey available right now!";
                Answers = null;
                HasMoreQuestions = false;
                return;
            }

            var providerId = userUsage.ProviderId;
            _userId = userUsage.UserId;
            var questionSeries = _service.GetProviderSurveyQuestion(providerId).OrderBy(i=>i.Order);
            _currentQuestionSeries = questionSeries.FirstOrDefault();

            if (_currentQuestionSeries == null)
            {
                QuestionText = "There is no survey available right now!";
                Answers = null;
                HasMoreQuestions = false;
                return;
            }

            _questions = _service.GetSurveyQuestions(_currentQuestionSeries.Id).OrderBy(i => i.Order).ToList();
            _currentQuestion = _questions.FirstOrDefault();

            if (_currentQuestion == null)
            {
                QuestionText = "There is no survey available right now!";
                HasMoreQuestions = false;
                Answers = null;
                return;
            }

            HasMoreQuestions = true;
            SetPropertiesForNextQuestion();
        }

        private void SetPropertiesForNextQuestion()
        {
            QuestionText = _currentQuestion.Text;

            Answers = _service.GetSurveyQuestionAnswers(_currentQuestion.Id).OrderBy(i => i.Order)
                .Select(i => new UserAnswerItem(i, _currentQuestion.CanHaveMoreThanOneAnswer)).ToList();

            IsMultiOptionQuestion = Answers.Count > 1;
            UserFreeAnswerToQuestion = null;
            IsSingleAnswer = !_currentQuestion.CanHaveMoreThanOneAnswer;
            AnswerIsFreeText = Answers.Count == 1 && !Answers.First().QuestionAnswer.IsPreDefined;
        }

        internal void Next()
        {
            SaveAnswer();
            var currentUserAnswers = Answers.Where(i => i.HasUserSelected).ToList();
            var nextQuestion = _questions.FirstOrDefault(i => i.Order > _currentQuestion.Order &&
                                                              (i.PreviousQuestionAnswerId == null || currentUserAnswers.Any(a=>a.QuestionAnswer.Id == i.PreviousQuestionAnswerId)));

            if (nextQuestion == null)
            {
                QuestionText = "Thanks for participating in the survey!";
                Answers = null;
                IsMultiOptionQuestion = false;
                UserFreeAnswerToQuestion = null;
                IsSingleAnswer = false;
                AnswerIsFreeText = false;
                HasMoreQuestions = false;
                IsBackButtonVisible = false;
                _previousQuestion = _currentQuestion;

                CreditWonText = $"You have won {_currentQuestionSeries.Credit:C} Smokeball credit.";

                return;
            }

            IsBackButtonVisible = true;
            _previousQuestion = _currentQuestion;
            _currentQuestion = nextQuestion;
            SetPropertiesForNextQuestion();
        }

        private void RemoveSurveyQuestions()
        {
            if (_currentQuestionSeries != null)
            {
                _service.DeleteUserAnsweredSurvey(_currentQuestionSeries.Id);
            }
        }

        private void SaveAnswer()
        {
            if ((_currentQuestion.CanHaveMoreThanOneAnswer || _answers.All(a => a.QuestionAnswer.IsPreDefined)) &&
                _answers.Any(a => a.HasUserSelected))
            {
                _service.SaveUserAnswer(_currentQuestion.ProviderSurveyQuestionId, _currentQuestion.Id, _userId, Answers
                    .Where(a => a.HasUserSelected)
                    .Select(a => a.QuestionAnswer).ToList());
            }
            else if (Answers.Any(a => !a.QuestionAnswer.IsPreDefined) &&
                     !string.IsNullOrWhiteSpace(UserFreeAnswerToQuestion))
            {
                _service.SaveUserFreeTextAnswer(_currentQuestion.ProviderSurveyQuestionId,
                    _currentQuestion.Id,
                    _userId,
                    Answers.FirstOrDefault(a => !a.QuestionAnswer.IsPreDefined)?.QuestionAnswer.Id,
                    UserFreeAnswerToQuestion);
            }

        }

        public void GoBack()
        {
            IsBackButtonVisible = false;
            HasMoreQuestions = true;
            _service.DeleteUserPreviousAnswer(_userId, _currentQuestion.Id);
            _currentQuestion = _previousQuestion;
            _previousQuestion = null;
            SetPropertiesForNextQuestion();
        }

        public void Close()
        {
            RemoveSurveyQuestions();
            Application.Current.Shutdown();
        }
    }
}
