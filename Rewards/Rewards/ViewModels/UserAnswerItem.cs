﻿using System;
using Entities;

namespace Rewards.ViewModels
{
    internal class UserAnswerItem: NotificationObject
    {
        private bool _hasUserSelected;

        public UserAnswerItem(SurveyQuestionAnswer questionAnswer, bool userCanSelectMulti)
        {
            QuestionAnswer = questionAnswer;
            UserCanSelectMulti = userCanSelectMulti;
        }

        public bool HasUserSelected
        {
            get => _hasUserSelected;
            set => SetValue(ref _hasUserSelected, value, nameof(HasUserSelected));
        }

        public bool UserCanSelectMulti { get; }

        public SurveyQuestionAnswer QuestionAnswer { get; }
    }
}
