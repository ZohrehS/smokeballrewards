﻿using System;
using System.Windows;
using Rewards.Services;
using Rewards.ViewModels;
using Rewards.Views;

namespace Rewards
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        QuestionsViewModel _currentSurveyViewModel;

        public MainWindow()
        {
            InitializeComponent();

            var service = new UserFeatureUsageService();
            service.InitializeQuestionAndAnswers();
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            var view = new QuestionsView();
            _currentSurveyViewModel = new QuestionsViewModel();
            view.DataContext = _currentSurveyViewModel;
            contentControl.Content = view;
            _currentSurveyViewModel.Initialize();
        }

        private void BtnClose_OnClick(object sender, RoutedEventArgs e)
        {
            if (_currentSurveyViewModel != null)
            {
                _currentSurveyViewModel.Close();
            }
            else
            {
                Application.Current.Shutdown();
            }
        }

        private void BtnMinimize_OnClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
    }
}
