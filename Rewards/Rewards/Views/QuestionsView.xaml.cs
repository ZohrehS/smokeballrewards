﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Rewards.ViewModels;

namespace Rewards.Views
{
    /// <summary>
    /// Interaction logic for QuestionsView.xaml
    /// </summary>
    public partial class QuestionsView : UserControl
    {
        public QuestionsView()
        {
            InitializeComponent();
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            ((QuestionsViewModel) DataContext).Next();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            ((QuestionsViewModel)DataContext).Close();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            ((QuestionsViewModel) DataContext).GoBack();
        }
    }
}
