﻿using System;

using System.Windows;
using System.Windows.Controls;
using Rewards.ViewModels;

namespace Rewards.Views
{
    public class AnswerItemDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate CheckboxDataTemplate { get; set; }
        public DataTemplate RadioButtonDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var answerItem = (UserAnswerItem) item;
            if (answerItem.UserCanSelectMulti)
                return CheckboxDataTemplate;

            return RadioButtonDataTemplate;

        }
    }
}
