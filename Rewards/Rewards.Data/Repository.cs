﻿using System;
using System.Collections.Generic;
using System.Data;
using Entities;

namespace Rewards.Data
{
    public class Repository
    {
        public const string
            SqlCreateSchema = @"create table if not exists UserSurveyAnswer (
                                       Id char(36) unique not null primary key,
                                       UserId char(36) not null,
                                       ProviderSurveyQuestionId char(36) not null,
                                       QuestionId char(36) not null,
                                       AnswerId char(36) not null,                                       
                                       Text text);

                                create table if not exists UserProviderUsage (
                                       Id char(36) unique not null primary key,
                                       ProviderId char(36) not null,
                                       UserId char(36) not null,
                                       Timestamp long not null);

                                create table if not exists ProviderSurveyQuestion (
                                       Id char(36) unique not null primary key,
                                       ProviderId char(36) not null,
                                       Sequence integer NOT NULL,
                                       Credit integer NOT NULL);

                                create table if not exists SurveyQuestion (
                                       Id char(36) unique not null primary key,
                                       ProviderSurveyQuestionId char(36) not null,
                                       Sequence integer NOT NULL,
                                       PreviousQuestionAnswerId char(36),
                                       Text text not null,
                                       CanHaveMoreThanOneAnswer text NOT NULL);

                                create table if not exists SurveyQuestionAnswer (
                                       Id char(36) unique not null primary key,                                       
                                       QuestionId char(36) not null,
                                       Sequence integer NOT NULL,                                       
                                       Text text null,
                                       IsPreDefined text NOT NULL);";
        public void AddUserAnswer(UserSurveyAnswer answer)
        {
            using (var sqlHelper = new SqlHelper())
            {
                sqlHelper.ExecuteNonQuery(CommandType.Text, SqlCreateSchema +
                    "insert or replace into UserSurveyAnswer " +
                    "(Id, UserId, ProviderSurveyQuestionId, QuestionId, AnswerId, Text) " +
                    "values (@Id, @UserId, @ProviderSurveyQuestionId, @QuestionId, @AnswerId, @Text);", 
                    sqlHelper.CreateParameter("@Id", answer.Id),
                    sqlHelper.CreateParameter("@UserId", answer.UserId),
                    sqlHelper.CreateParameter("@ProviderSurveyQuestionId", answer.ProviderSurveyQuestionId),
                    sqlHelper.CreateParameter("@QuestionId", answer.QuestionId),
                    sqlHelper.CreateParameter("@AnswerId", answer.AnswerId),
                    sqlHelper.CreateParameter("@Text", answer.Text));
            }
        }

        public void DeleteUserAnswer(string userId, string questionId)
        {
            using (var sqlHelper = new SqlHelper())
            {
                sqlHelper.ExecuteNonQuery(CommandType.Text, SqlCreateSchema +
                    "Delete from UserSurveyAnswer where UserId = @UserId and QuestionId = @QuestionId;",
                    sqlHelper.CreateParameter("@UserId", userId),
                    sqlHelper.CreateParameter("@QuestionId", questionId));
            }
        }

        public void AddUserUsage(UserProviderUsage usage)
        {
            using (var sqlHelper = new SqlHelper())
            {
                sqlHelper.ExecuteNonQuery(CommandType.Text, SqlCreateSchema +
                    "insert or replace into UserProviderUsage " +
                    "(Id, ProviderId, UserId, Timestamp) " +
                    "values (@Id, @ProviderId, @UserId, @Timestamp);",
                    sqlHelper.CreateParameter("@Id", usage.Id),
                    sqlHelper.CreateParameter("@ProviderId", usage.ProviderId),
                    sqlHelper.CreateParameter("@UserId", usage.UserId),
                    sqlHelper.CreateParameter("@Timestamp", DateTime.UtcNow.Ticks));
            }
        }

        public void DeleteUserUsage(string usageId)
        {
            using (var sqlHelper = new SqlHelper())
            {
                sqlHelper.ExecuteNonQuery(CommandType.Text, SqlCreateSchema + 
                    "Delete from UserProviderUsage where Id = @Id;",
                    sqlHelper.CreateParameter("@Id", usageId));
            }
        }

        public List<UserProviderUsage> GetAllUserUsage()
        {
            var result = new List<UserProviderUsage>();

            using (var sqlHelper = new SqlHelper())
            {
                using (var reader = sqlHelper.ExecuteReader(CommandType.Text, SqlCreateSchema +
                    "select * from UserProviderUsage"))
                {
                    while (reader.Read())
                    {
                        var item = new UserProviderUsage();
                        item.Id = sqlHelper.GetField<string>(reader, "Id");
                        item.ProviderId = sqlHelper.GetField<string>(reader, "ProviderId");
                        item.UserId = sqlHelper.GetField<string>(reader, "UserId");
                        item.Timestamp = new DateTime(sqlHelper.GetField<long>(reader, "Timestamp"), DateTimeKind.Utc);
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        public List<ProviderSurveyQuestion> GetProviderQuestions(string providerId)
        {
            var result = new List<ProviderSurveyQuestion>();

            using (var sqlHelper = new SqlHelper())
            {

                using (var reader = sqlHelper.ExecuteReader(CommandType.Text, SqlCreateSchema +
                                                                              "select * from ProviderSurveyQuestion where ProviderId = @providerId",
                    sqlHelper.CreateParameter("@providerId", providerId)))
                {
                    while (reader.Read())
                    {
                        var item = new ProviderSurveyQuestion();
                        item.Id = sqlHelper.GetField<string>(reader, "Id");
                        item.ProviderId = sqlHelper.GetField<string>(reader, "ProviderId");
                        item.Order = (int)sqlHelper.GetField<long>(reader, "Sequence");
                        item.Credit = (int)sqlHelper.GetField<long>(reader, "Credit");
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        public List<SurveyQuestion> GetSurveyQuestionsForProvider(string providerQuestionId)
        {
            var result = new List<SurveyQuestion>();

            using (var sqlHelper = new SqlHelper())
            {

                using (var reader = sqlHelper.ExecuteReader(CommandType.Text, SqlCreateSchema +
                                                                              "select * from SurveyQuestion where ProviderSurveyQuestionId = @providerQuestionId",
                    sqlHelper.CreateParameter("@providerQuestionId", providerQuestionId)))
                {
                    while (reader.Read())
                    {
                        var item = new SurveyQuestion();
                        item.Id = sqlHelper.GetField<string>(reader, "Id");
                        item.ProviderSurveyQuestionId = sqlHelper.GetField<string>(reader, "ProviderSurveyQuestionId");
                        item.Order = (int)sqlHelper.GetField<long>(reader, "Sequence");
                        item.PreviousQuestionAnswerId = sqlHelper.GetField<string>(reader, "PreviousQuestionAnswerId");
                        item.Text = sqlHelper.GetField<string>(reader, "Text");
                        item.CanHaveMoreThanOneAnswer = bool.Parse(sqlHelper.GetField<string>(reader, "CanHaveMoreThanOneAnswer"));
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        public List<SurveyQuestionAnswer> GetSurveyQuestionAnswers(string questionId)
        {
            var result = new List<SurveyQuestionAnswer>();

            using (var sqlHelper = new SqlHelper())
            {

                using (var reader = sqlHelper.ExecuteReader(CommandType.Text, SqlCreateSchema +
                                                                              "select * from SurveyQuestionAnswer where QuestionId = @QuestionId",
                    sqlHelper.CreateParameter("@QuestionId", questionId)))
                {
                    while (reader.Read())
                    {
                        var item = new SurveyQuestionAnswer();
                        item.Id = sqlHelper.GetField<string>(reader, "Id");
                        item.QuestionId = sqlHelper.GetField<string>(reader, "QuestionId");
                        item.Order = (int)sqlHelper.GetField<long>(reader, "Sequence");
                        item.IsPreDefined = bool.Parse(sqlHelper.GetField<string>(reader, "IsPreDefined"));
                        item.Text = sqlHelper.GetField<string>(reader, "Text");
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        private void AddProviderQuestion(ProviderSurveyQuestion questionSeries)
        {
            using (var sqlHelper = new SqlHelper())
            {
                sqlHelper.ExecuteNonQuery(CommandType.Text, SqlCreateSchema +
                                                            "insert or replace into ProviderSurveyQuestion " +
                                                            "(Id, ProviderId, Sequence, Credit) " +
                                                            "values (@Id, @ProviderId, @Order, @Credit);",
                    sqlHelper.CreateParameter("@Id", questionSeries.Id),
                    sqlHelper.CreateParameter("@ProviderId", questionSeries.ProviderId),
                    sqlHelper.CreateParameter("@Order", questionSeries.Order),
                    sqlHelper.CreateParameter("@Credit", questionSeries.Credit));
            }
        }

        private void AddSurveyQuestion(SurveyQuestion question)
        {
            using (var sqlHelper = new SqlHelper())
            {
                sqlHelper.ExecuteNonQuery(CommandType.Text, SqlCreateSchema +
                    "insert or replace into SurveyQuestion " +
                    "(Id, ProviderSurveyQuestionId, Sequence, PreviousQuestionAnswerId, Text, CanHaveMoreThanOneAnswer) " +
                    "values (@Id, @ProviderSurveyQuestionId, @Order, @PreviousQuestionAnswerId, @Text, @CanHaveMultiAnswer);",
                    sqlHelper.CreateParameter("@Id", question.Id),
                    sqlHelper.CreateParameter("@ProviderSurveyQuestionId", question.ProviderSurveyQuestionId),
                    sqlHelper.CreateParameter("@Order", question.Order),
                    sqlHelper.CreateParameter("@PreviousQuestionAnswerId", question.PreviousQuestionAnswerId),
                    sqlHelper.CreateParameter("@Text", question.Text),
                    sqlHelper.CreateParameter("@CanHaveMultiAnswer", question.CanHaveMoreThanOneAnswer.ToString()));
            }
        }

        private void AddSurveyAnswer(SurveyQuestionAnswer answer)
        {
            using (var sqlHelper = new SqlHelper())
            {
                sqlHelper.ExecuteNonQuery(CommandType.Text, SqlCreateSchema +
                    "insert or replace into SurveyQuestionAnswer " +
                    "(Id, QuestionId, Sequence, Text, IsPreDefined) " +
                    "values (@Id, @QuestionId, @Order, @Text, @IsPreDefined);",
                    sqlHelper.CreateParameter("@Id", answer.Id),
                    sqlHelper.CreateParameter("@QuestionId", answer.QuestionId),
                    sqlHelper.CreateParameter("@Order", answer.Order),
                    sqlHelper.CreateParameter("@Text", answer.Text),
                    sqlHelper.CreateParameter("@IsPreDefined", answer.IsPreDefined.ToString()));
            }
        }

        public void DeleteAnsweredSurvey(string questionSeriesId)
        {
            using (var sqlHelper = new SqlHelper())
            {
                sqlHelper.ExecuteNonQuery(CommandType.Text, SqlCreateSchema +
                                                            "Delete from ProviderSurveyQuestion where Id = @Id;",
                    sqlHelper.CreateParameter("@Id", questionSeriesId));
            }
        }


        private bool IsDbEmpty()
        {
            UserProviderUsage item = null;

            using (var sqlHelper = new SqlHelper())
            {
                using (var reader = sqlHelper.ExecuteReader(CommandType.Text, SqlCreateSchema +
                                                                              "select * from SurveyQuestion limit 1;"))
                {
                    if (reader.Read())
                    {
                        item = new UserProviderUsage();
                        item.Id = sqlHelper.GetField<string>(reader, "Id");
                        item.ProviderId = sqlHelper.GetField<string>(reader, "ProviderId");
                        item.UserId = sqlHelper.GetField<string>(reader, "UserId");
                        item.Timestamp = new DateTime(sqlHelper.GetField<long>(reader, "Timestamp"), DateTimeKind.Utc);
                    }
                }
            }

            return item?.Id == null;

        }

        public bool IsSurveyAvailableForProvider(string userId, string providerId)
        {
            ProviderSurveyQuestion item = null;

            using (var sqlHelper = new SqlHelper())
            {
                using (var reader = sqlHelper.ExecuteReader(CommandType.Text, SqlCreateSchema +
                                                                              "select * from ProviderSurveyQuestion where ProviderId = @ProviderId limit 1;",
                    sqlHelper.CreateParameter("@ProviderId", providerId)))
                {
                    if (reader.Read())
                    {
                        item = new ProviderSurveyQuestion();
                        item.Id = sqlHelper.GetField<string>(reader, "Id");
                        item.ProviderId = sqlHelper.GetField<string>(reader, "ProviderId");
                    }
                }
            }

            return item?.Id != null;
        }

        public void InitializeQuestionAndAnswers()
        {
            if (!IsDbEmpty())
            {
                return;
            }

            var questionSeries = new ProviderSurveyQuestion
            {
                Id = Guid.NewGuid().ToString(),
                ProviderId = "TaskingV2",
                Order = 0,
                Credit = 10
            };

            var question1 = new SurveyQuestion
            {
                Id = Guid.NewGuid() .ToString(),
                ProviderSurveyQuestionId = questionSeries.Id,
                Order = 0,
                Text = "Did you like the new Tasks view?",
                CanHaveMoreThanOneAnswer = false
            };

            var answer11 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question1.Id,
                Order = 0,
                Text = "Yes",
                IsPreDefined = true
            };

            var answer12 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question1.Id,
                Order = 1,
                Text = "No",
                IsPreDefined = true
            };

            var question2 = new SurveyQuestion
            {
                Id = Guid.NewGuid().ToString(),
                ProviderSurveyQuestionId = questionSeries.Id,
                Order = 1,
                Text = "Why you didn't like it?",
                PreviousQuestionAnswerId = answer12.Id,
                CanHaveMoreThanOneAnswer = false
            };

            var answer21 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question2.Id,
                Order = 0,
                IsPreDefined = false
            };

            var question3 = new SurveyQuestion
            {
                Id = Guid.NewGuid().ToString(),
                ProviderSurveyQuestionId = questionSeries.Id,
                Order = 2,
                Text = "What did you like most?",
                PreviousQuestionAnswerId = answer11.Id,
                CanHaveMoreThanOneAnswer = true
            };

            var answer31 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question3.Id,
                Order = 0,
                IsPreDefined = true,
                Text = "The loading of tasks was very fast."
            };

            var answer32 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question3.Id,
                Order = 1,
                IsPreDefined = true,
                Text = "The Default Filters was very helpful."
            };

            var answer33 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question3.Id,
                Order = 2,
                IsPreDefined = true,
                Text = "The Custom Filters was awesome."
            };

            var question4 = new SurveyQuestion
            {
                Id = Guid.NewGuid().ToString(),
                ProviderSurveyQuestionId = questionSeries.Id,
                Order = 4,
                Text = "What did cause you to use the Custom Filters?",
                PreviousQuestionAnswerId = answer33.Id,
                CanHaveMoreThanOneAnswer = true
            };

            var answer41 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question4.Id,
                Order = 0,
                IsPreDefined = true,
                Text = "The Default Filters is missing filtering by due date."
            };

            var answer42 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question4.Id,
                Order = 1,
                IsPreDefined = true,
                Text = "The Default Filters has no option to exclude overdue tasks."
            };

            var answer43 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question4.Id,
                Order = 2,
                IsPreDefined = true,
                Text = "The Default Filters has no option to exclude Waiting For Due Date."
            };

            var answer44 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question4.Id,
                Order = 3,
                IsPreDefined = true,
                Text = "I had a complex set of filters that cannot be done by Default Filters ."
            };

            AddProviderQuestion(questionSeries);

            AddSurveyQuestion(question1);
            AddSurveyQuestion(question2);
            AddSurveyQuestion(question3);
            AddSurveyQuestion(question4);

            AddSurveyAnswer(answer11);
            AddSurveyAnswer(answer12);

            AddSurveyAnswer(answer21);

            AddSurveyAnswer(answer31);
            AddSurveyAnswer(answer32);
            AddSurveyAnswer(answer33);

            AddSurveyAnswer(answer41);
            AddSurveyAnswer(answer42);
            AddSurveyAnswer(answer43);
            AddSurveyAnswer(answer44);


            var questionSeries2 = new ProviderSurveyQuestion
            {
                Id = Guid.NewGuid().ToString(),
                ProviderId = "TaskingV2",
                Order = 1,
                Credit = 5
            };

            question1 = new SurveyQuestion
            {
                Id = Guid.NewGuid().ToString(),
                ProviderSurveyQuestionId = questionSeries2.Id,
                Order = 0,
                Text = "Did you use the new Tasks?",
                CanHaveMoreThanOneAnswer = false
            };

            answer11 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question1.Id,
                Order = 0,
                Text = "Yes",
                IsPreDefined = true
            };

            answer12 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question1.Id,
                Order = 1,
                Text = "No",
                IsPreDefined = true
            };

            question2 = new SurveyQuestion
            {
                Id = Guid.NewGuid().ToString(),
                ProviderSurveyQuestionId = questionSeries2.Id,
                Order = 1,
                Text = "Did the list load in an acceptable time?",
                CanHaveMoreThanOneAnswer = false,
                PreviousQuestionAnswerId = answer11.Id
            };

            answer21 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question2.Id,
                Order = 0,
                Text = "Yes",
                IsPreDefined = true
            };

            var answer22 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question2.Id,
                Order = 1,
                Text = "No",
                IsPreDefined = true
            };

            question3 = new SurveyQuestion
            {
                Id = Guid.NewGuid().ToString(),
                ProviderSurveyQuestionId = questionSeries2.Id,
                Order = 2,
                Text = "Do you use mostly the Default Filters or Custom Filters?",
                CanHaveMoreThanOneAnswer = false,
                PreviousQuestionAnswerId = null
            };

            answer31 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question3.Id,
                Order = 0,
                Text = "Default Filters",
                IsPreDefined = true
            };

            answer32 = new SurveyQuestionAnswer()
            {
                Id = Guid.NewGuid().ToString(),
                QuestionId = question3.Id,
                Order = 1,
                Text = "Custom Filters",
                IsPreDefined = true
            };

            AddProviderQuestion(questionSeries2);

            AddSurveyQuestion(question1);
            AddSurveyQuestion(question2);
            AddSurveyQuestion(question3);

            AddSurveyAnswer(answer11);
            AddSurveyAnswer(answer12);

            AddSurveyAnswer(answer21);
            AddSurveyAnswer(answer22);

            AddSurveyAnswer(answer31);
            AddSurveyAnswer(answer32);

        }

    }
}
