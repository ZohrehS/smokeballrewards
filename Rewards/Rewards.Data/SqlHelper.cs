﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Rewards.Data
{
    public class SqlHelper : IDisposable
    {
        private readonly SqliteConnectionWrapper _connection;

        public SqlHelper()
        {
            _connection = GetConnection();
        }

        private SqliteConnectionWrapper GetConnection()
        {
            var builder = new SQLiteConnectionStringBuilder();
            builder.JournalMode = SQLiteJournalModeEnum.Wal;
            builder.ReadOnly = false;
            builder.DataSource = @"F:\repos\smokeballrewards\Rewards\Rewards.db";
            builder.Version = 3;

            if (!File.Exists(builder.DataSource))
                SQLiteConnection.CreateFile(builder.DataSource);

            var sqlite = new SQLiteConnection(builder.ConnectionString);
            return new SqliteConnectionWrapper(sqlite);
        }

        public IDataReader ExecuteReader(CommandType cmdType, string cmdText, params DbParameter[] commandParameters)
        {
            SQLiteDataReader reader;

            using (var cmd = new SQLiteCommand())
            {
                PrepareCommand(cmd, cmdType, cmdText, commandParameters);

                reader = cmd.ExecuteReader();
            }

            return reader;
        }

        public void ExecuteNonQuery(CommandType cmdType, string cmdText, params DbParameter[] commandParameters)
        {
            using (var cmd = new SQLiteCommand())
            {
                PrepareCommand(cmd, cmdType, cmdText, commandParameters);

                cmd.ExecuteNonQuery();
            }
        }

        public object ExecuteScalar(CommandType cmdType, string cmdText, params DbParameter[] commandParameters)
        {
            object result;
            using (var cmd = new SQLiteCommand())
            {
                PrepareCommand(cmd, cmdType, cmdText, commandParameters);

                result = cmd.ExecuteScalar();
            }

            return result;
        }


        /// <summary>
        ///     Prepare a command for execution
        /// </summary>
        /// <param name="cmd">SqlCommand object</param>
        /// <param name="conn">SqlConnection object</param>
        /// <param name="cmdType">Cmd type e.g. stored procedure or text</param>
        /// <param name="cmdText">Command text, e.g. Select * from FreeForms</param>
        /// <param name="cmdParms">SqlParameters to use in the command</param>
        private void PrepareCommand(DbCommand cmd, CommandType cmdType, string cmdText, IEnumerable<DbParameter> cmdParms)
        {
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }

            cmd.Connection = _connection.Connection;
            cmd.CommandText = cmdText;
            cmd.CommandType = cmdType;
            cmd.Parameters.Clear();

            if (cmdParms == null) return;
            foreach (var parm in cmdParms)
                cmd.Parameters.Add(parm);
        }

        public void LoadJsonExtension()
        {
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }

            _connection.EnableExtensions(true);
            _connection.LoadExtension("System.Data.SQLite.dll", "sqlite3_json_init");
        }

        public SQLiteParameter CreateParameter(string name, string value)
        {
            var p = new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.String
            };

            if (value == null)
            {
                p.Value = DBNull.Value;
            }
            else
            {
                p.Value = value;
            }

            return p;
        }

        public SQLiteParameter CreateParameter(string name, DateTime? value)
        {
            var p = new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.DateTime
            };

            if (value == null)
            {
                p.Value = DBNull.Value;
            }
            else
            {
                p.Value = value;
            }

            return p;
        }

        public SQLiteParameter CreateParameter(string name, byte[] value)
        {
            return new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.Binary,
                Value = value
            };
        }

        public SQLiteParameter CreateParameter(string name, DateTime value)
        {
            return new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.DateTime,
                Value = value
            };
        }

        public SQLiteParameter CreateParameter(string name, decimal value)
        {
            return new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.Decimal,
                Value = value
            };
        }

        public SQLiteParameter CreateParameter(string name, long value)
        {
            return new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.Int64,
                Value = value
            };
        }

        public SQLiteParameter CreateParameter(string name, long? value)
        {
            var p = new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.Int64
            };

            if (value.HasValue)
            {
                p.Value = value.Value;
            }
            else
            {
                p.Value = DBNull.Value;
            }

            return p;
        }

        public SQLiteParameter CreateParameter(string name, bool value)
        {
            return new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.Boolean,
                Value = value
            };
        }


        public SQLiteParameter CreateParameter(string name, bool? value)
        {
            var p = new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.Boolean
            };

            if (value.HasValue)
            {
                p.Value = value.Value;
            }
            else
            {
                p.Value = DBNull.Value;
            }

            return p;
        }

        public SQLiteParameter CreateParameter(string name, int value)
        {
            return new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.Int32,
                Value = value
            };
        }

        public SQLiteParameter CreateParameter(string name, int? value)
        {
            var p = new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.Int32
            };

            if (value.HasValue)
            {
                p.Value = value.Value;
            }
            else
            {
                p.Value = DBNull.Value;
            }

            return p;
        }

        public SQLiteParameter CreateParameter(string name, Guid value)
        {
            return new SQLiteParameter
            {
                ParameterName = name,
                DbType = DbType.String,
                Value = value
            };
        }

        public T GetField<T>(IDataReader reader, string name)
        {
            return GetField<T>(reader, reader.GetOrdinal(name));
        }

        public T GetField<T>(IDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? default(T) : (T)reader.GetValue(ordinal);
        }

        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}
