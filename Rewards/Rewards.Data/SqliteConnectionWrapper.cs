﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rewards.Data
{
    internal class SqliteConnectionWrapper
    {
            public SqliteConnectionWrapper(SQLiteConnection sqLiteConnection)
            {
                Connection = sqLiteConnection;
            }

            public SQLiteConnection Connection { get; }

            public ConnectionState State => Connection.State;

            public void Dispose()
            {
                Connection?.Dispose();
            }

            public void Open()
            {
                Connection.Open();
            }

            public void EnableExtensions(bool enable)
            {
                Connection.EnableExtensions(enable);
            }

            public void LoadExtension(string fileName, string procName)
            {
                Connection.LoadExtension(fileName, procName);
            }

            public DbTransaction BeginTransaction()
            {
                return Connection.BeginTransaction();
            }
        
    }
}
